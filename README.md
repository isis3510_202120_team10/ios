# iOS

Repository that contains the iOS version of team 10's app

## How to run the app

### Setting up

Clone the repository and run pod install inside the folder WikiWiki. 
To open on XCode open WikiWiki.xcworkspace. Not WikiWiki.xcodeproj.

### Running the app

Build the app and install it in your device.

You can create an account using the "Sign Up with email" button or use the following login credentials:
**Note:** You will not be able to sign up unless you select an image.

Username: pietroehrlich@gmail.com
Password: Testing123

### Using the screens

We currently have the "No delivery in progress" and "Current delivery" screens separate although they are shown depending on whether the driver has a delivery assigned to him at the moment. This makes it easier to test the app since both screens can be tested at the same time.

##### Current Delivery screen

Due to the fact that we are using Apple MapKit to calculate routes we only calculate routes in the USA. Because of this, you cannot test the Routes feature in which a route to the destination is automatically calculated unless you spoof your location. You can Spoof you location in IOS using [iAnyGo](https://www.tenorshare.com/products/ianygo-change-gps-location-iphone.html). We recommend that you spoof your location to a place in Manhattan (close to Central Park) since the destintation is located [here](https://goo.gl/maps/p5db7fwKxv7tjszD9).

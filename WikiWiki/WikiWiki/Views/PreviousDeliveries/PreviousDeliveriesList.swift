//
//  PreviousDeliveriesList.swift
//  WikiWiki
//
//  Created by Carlos Infante on 8/10/21.
//

import SwiftUI
import UIKit

struct DeliveryCard: View {
     
    var date: String
    var driver: String
    var time: String
    var textSize: CGFloat = 13
    var imageSize:CGFloat = 70
    @StateObject var delivery = CurrentDeliveryData()
    
    
    var body: some View {
        HStack {
            Spacer()
            Image("apple")
                .resizable()
                .frame(width: imageSize, height: imageSize)
                .clipShape(Circle())
            //                .aspectRatio(contentMode: .fit)
            VStack(alignment: .leading, spacing: 10) {
                Spacer()
                HStack{
                    Image(systemName: "calendar")
                        .foregroundColor(Color("PrimaryColor"))
                    Text("Date:")
                        .fontWeight(.bold)
                        .font(.system(size: textSize))
                        .foregroundColor(Color("PrimaryColor"))
                    Text(date)
                        .font(.system(size: textSize))
                }
                HStack{
                    Image(systemName: "person")
                        .foregroundColor(Color("PrimaryColor"))
                    Text("Client:")
                        .fontWeight(.bold)
                        .font(.system(size: textSize))
                        .foregroundColor(Color("PrimaryColor"))
                    Text(driver)
                        .font(.system(size: textSize))
                }
                HStack{
                    Image(systemName: "clock")
                        .foregroundColor(Color("PrimaryColor"))
                    Text("Time spent:")
                        .fontWeight(.bold)
                        .font(.system(size: textSize))
                        .foregroundColor(Color("PrimaryColor"))
                    Text(time + " min")
                        .font(.system(size: textSize))
                }
                Spacer()
            }
            .frame(maxWidth: .infinity)
            Spacer()
        }
        .cornerRadius(10)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color(.sRGB, red: 150/255, green: 150/255, blue: 150/255, opacity: 0.3), lineWidth: 1)
        )
        .frame(maxWidth: .infinity)
        .edgesIgnoringSafeArea([.leading, .trailing])
    }
}


struct PreviousDeliveriesList: View {
    @State private var searchText = ""
    @StateObject private var deliveries = PreviousDeliveryData()
    @State var hasNoInternet: Bool = !NetworkMonitor.shared.isConnected
    let dateFormatter = DateFormatter()
    
    var body: some View {
        NavigationView {
            VStack {
            if hasNoInternet {
                    HStack {
                        // Banner Content
                        VStack(alignment: .leading, spacing: 2) {
                            Text("Connection error")
                                .bold()
                            Text("Network unavailable, please try again later")
                                .font(Font.system(size: 15, weight: Font.Weight.light, design: Font.Design.default))
                        }
                        .foregroundColor(.white)
                        Spacer()
                    }

                .padding(12)
                .background(Color.red)
                .cornerRadius(8)
            }
                List(searchResults, id: \.self) { delivery in
                    NavigationLink(destination: DeliveryDetailView(orderID: delivery.orderID)) {
                        DeliveryCard(date: delivery.dateString, driver: delivery.clientName, time: String(delivery.duration))                        
                    }
                }
                .navigationTitle("Previous Deliveries")
                .listStyle(PlainListStyle())
            }
            
        }
        .navigationBarHidden(true)
        .accentColor(Color.white)
        .searchable(text: $searchText)
        .onChange(of: hasNoInternet) {[hasNoInternet] newState in
            if !hasNoInternet {
                deliveries.fetchAllDeliveries()
            }
        }
        .onAppear() {
            guard NetworkMonitor.shared.isConnected else {
                hasNoInternet = true
                deliveries.load()
                return
            }
            
            if hasNoInternet {
                hasNoInternet = false
            }
            else {
                deliveries.fetchAllDeliveries()
            }
        }
        
    }
    
    var searchResults: [PrevDelivery] {
        if searchText.isEmpty {
            return deliveries.prevDeliveries
        } else {
            return deliveries.prevDeliveries.filter { $0.clientName.contains(searchText) }
        }
    }
}

struct PreviousDeliveriesList_Previews: PreviewProvider {
    static var previews: some View {
        PreviousDeliveriesList()
    }
}

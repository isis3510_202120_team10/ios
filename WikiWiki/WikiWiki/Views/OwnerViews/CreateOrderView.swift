//
//  CreateOrderView.swift
//  WikiWiki
//
//  Created by Jorge Andrés Esguerra Alarcón on 12/7/21.
//

import SwiftUI

struct CreateOrderView: View {
    @State var clientName = ""
    @State var phoneNumber = ""
    @State var destinationAddress = ""
    @State var product = ""
    @State var deliveryPrice = UserDefaults.standard.string(forKey: "defaultPrice") ?? ""
    
    @StateObject var orderViewModel = OrderViewModel()
    @State private var showingAlert = false
    @State private var showingConfirmation = false
    @State private var showingError = false
    
    @State private var checked = false
    @State private var overwriteDefaultPrice = false
    
    var body: some View {
        VStack {
            Text("Client Information")
                .font(.headline)
                .foregroundColor(.primary)
            
            TextField("Client's Full Name", text: $clientName)
                .padding(10)
                .overlay(RoundedRectangle(cornerRadius: 12.0).strokeBorder(Color("PrimaryColor"), style: StrokeStyle(lineWidth: 1.0)))
                .disableAutocorrection(true)
                .autocapitalization(.none)
                .padding(.horizontal)
            TextField("Phone Number", text: $phoneNumber)
                .padding(10)
                .overlay(RoundedRectangle(cornerRadius: 12.0).strokeBorder(Color("PrimaryColor"), style: StrokeStyle(lineWidth: 1.0)))
                .disableAutocorrection(true)
                .autocapitalization(.none)
                .padding(.horizontal)
                .padding(.bottom)
            
            Text("Delivery Information")
                .font(.headline)
                .foregroundColor(.primary)
            
            TextField("Destination Address", text: $destinationAddress)
                .padding(10)
                .overlay(RoundedRectangle(cornerRadius: 12.0).strokeBorder(Color("PrimaryColor"), style: StrokeStyle(lineWidth: 1.0)))
                .disableAutocorrection(true)
                .autocapitalization(.none)
                .padding(.horizontal)
            
            TextField("Product", text: $product)
                .padding(10)
                .overlay(RoundedRectangle(cornerRadius: 12.0).strokeBorder(Color("PrimaryColor"), style: StrokeStyle(lineWidth: 1.0)))
                .disableAutocorrection(true)
                .autocapitalization(.none)
                .padding(.horizontal)
            
            TextField("Delivery Cost", text: $deliveryPrice)
                .padding(10)
                .overlay(RoundedRectangle(cornerRadius: 12.0).strokeBorder(Color("PrimaryColor"), style: StrokeStyle(lineWidth: 1.0)))
                .disableAutocorrection(true)
                .autocapitalization(.none)
                .padding(.horizontal)
            
            
            HStack {
                CheckBoxView(checked: $checked)
                Text("Will pay upon delivered")
            }
            HStack {
                CheckBoxView(checked: $overwriteDefaultPrice)
                Text("Overwrite default price")
            }
            
            Button(action: {
                
                guard NetworkMonitor.shared.isConnected else {
                    showingAlert = true
                    showingConfirmation = false
                    return
                }
                
                guard !clientName.isEmpty, !phoneNumber.isEmpty, !destinationAddress.isEmpty,
                      !product.isEmpty, !deliveryPrice.isEmpty else {
                    showingError = true
                    return
                }
                
                
                orderViewModel.createOrder(clientName: clientName, phoneNumber: phoneNumber, destinationAddress: destinationAddress, product: product, payOnDelivery: checked, deliveryPrice: deliveryPrice)
                showingConfirmation = true
                UserDefaults.standard.set(deliveryPrice, forKey: "defaultPrice")
            }, label: {
                Text("Create Order")
                    .foregroundColor(Color.white)
                    .frame(maxWidth: UIScreen.main.bounds.width, minHeight: 40)
                    .background(Color.blue)
            })
                .alert("Network Unavailable, please try again later", isPresented: $showingAlert) {
                    Button("OK", role: .cancel) { }
                }
                .alert("Order successfully created!", isPresented: $showingConfirmation) {
                    Button("OK", role: .cancel) { }
                }
                .alert("Oops! Fill all fields and try again", isPresented: $showingError) {
                    Button("OK", role: .cancel) { }
                }
                .cornerRadius(15)
                .padding(.top)
                .padding(.horizontal)
        }
    }

    

}


struct CreateOrderView_Previews: PreviewProvider {
    static let viewModel = AuthViewModel()
    static var previews: some View {
        CreateOrderView()
            .environmentObject(viewModel)
    }
}


/*
 Following CheckBox View taken from https://stackoverflow.com/questions/58425829/how-can-i-create-a-text-with-checkbox-in-swiftui
 Top answer by https://stackoverflow.com/users/3820660/hello-world
 */

struct CheckBoxView: View {
    @Binding var checked: Bool

    var body: some View {
        Image(systemName: checked ? "checkmark.square.fill" : "square")
            .foregroundColor(checked ? Color(UIColor.systemBlue) : Color.secondary)
            .onTapGesture {
                self.checked.toggle()
            }
    }
}

struct CheckBoxView_Previews: PreviewProvider {
    struct CheckBoxViewHolder: View {
        @State var checked = false

        var body: some View {
            CheckBoxView(checked: $checked)
        }
    }

    static var previews: some View {
        CheckBoxViewHolder()
    }
}

//
//  TabBar.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 8/10/21.
//

import SwiftUI

struct TabBar: View {
    
    var body: some View {
        TabView {
            PreviousDeliveriesList()
                .tabItem {
                    Label("Previous deliveries", systemImage: "bag")
                }
            CurrentDeliveryEmptyView()
                .tabItem {
                    Label("Empty delivery", systemImage: "circle")
                }
            CurrentDeliveryActive()
                .tabItem {
                    Label("Current delivery", systemImage: "list.bullet")
                }
            Profile()
                .tabItem {
                    Label("Profile", systemImage: "person")
                }
        }
        .accentColor(Color("PrimaryColor"))
    }
}

struct TabBar_Previews: PreviewProvider {
    static let viewModel = AuthViewModel()
    
    static var previews: some View {
        TabBar()
            .environmentObject(viewModel)
    }
}

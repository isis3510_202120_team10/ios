//
//  Profile.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich & Jorge Esguerra on 8/10/21.
//

import SwiftUI

struct Profile: View {
    @EnvironmentObject var viewModel: AuthViewModel
    
    var body: some View {
        VStack {
            Text("Your Profile")
                .font(.headline)
                .foregroundColor(.primary)
            
            AsyncImage(
                url: URL(string: viewModel.userProfile?.photoUrl ?? viewModel.cache.object(forKey: "userProfile")?.photoUrl ?? "https://via.placeholder.com/500x400"),
                content: { image in
                    image.resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(maxWidth: 500, maxHeight: 400)
                },
                placeholder: {
                    ProgressView()
                }
                
            )
                .frame(maxWidth: 500, maxHeight: 400)
            DataFieldView(description: "Name: ", content: viewModel.userProfile?.name ?? viewModel.cache.object(forKey: "userProfile")?.name ?? "N/A")
            DataFieldView(description: "Last Name: ", content: viewModel.userProfile?.lastName ?? viewModel.cache.object(forKey: "userProfile")?.lastName ?? "N/A")
            DataFieldView(description: "Phone Number: ", content: viewModel.userProfile?.phoneNumber ?? viewModel.cache.object(forKey: "userProfile")?.phoneNumber ?? "N/A")
            DataFieldView(description: "Role type: ", content: viewModel.userProfile?.role ?? viewModel.cache.object(forKey: "userProfile")?.role ?? "N/A")
            
            Button(action: {
                viewModel.signOut()
            }, label: {
                Text("Sign Out")
                    .foregroundColor(Color.white)
                    .frame(maxWidth: UIScreen.main.bounds.width, minHeight: 40)
                    .background(Color("PrimaryColor")) // este viene de paleta de colores nuestra.
            })
                .cornerRadius(15)
                .padding(.top)
                .padding(.horizontal)
        }
        
    }
}


struct DataFieldView: View {
    var description: String
    var content: String?
    var body: some View {
        Label {
            Text(description)
                .font(.headline)
                .foregroundColor(.primary)
            Text(content ?? "N/A")
                .font(.subheadline)
                .foregroundColor(.secondary)
        } icon: {
            Circle()
                .frame(width: 10, height: 10, alignment: .center)
                .foregroundColor(Color("PrimaryColor"))
        }
    }
}



struct Profile_Previews: PreviewProvider {
    static var previews: some View {
        Profile()
    }
}

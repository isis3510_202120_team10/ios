//
//  SignInView.swift
//  WikiWiki
//
//  Created by Jorge Andrés Esguerra Alarcón on 10/7/21.
//

import SwiftUI
import FirebaseAuth


struct SignInView: View {
    @State var email = ""
    @State var password = ""
    
    @EnvironmentObject var viewModel: AppViewModel
    var body: some View {
        
        VStack {
            Image("WikiWikiLogo")
                .resizable()
                .scaledToFit()
                .aspectRatio(2/3, contentMode: .fit)
                .padding(.bottom)
            
            VStack {
                TextField("Email Address", text: $email)
                    .padding(10)
                    .overlay(RoundedRectangle(cornerRadius: 12.0).strokeBorder(Color("PrimaryColor"), style: StrokeStyle(lineWidth: 1.0)))
                    //.padding(10)
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
                    .padding(.horizontal)
                
                
                SecureField("Password", text: $password)
                    .padding(10)
                    .overlay(RoundedRectangle(cornerRadius: 12.0).strokeBorder(Color("PrimaryColor"), style: StrokeStyle(lineWidth: 1.0)))
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
                    .padding(.horizontal)
                
                
                Button(action: {
                    guard !email.isEmpty, !password.isEmpty else {
                        return
                    }
                    viewModel.signIn(email: email, password: password)
                }, label: {
                    Text("Sign In")
                        .foregroundColor(Color.white)
                        .frame(maxWidth: UIScreen.main.bounds.width, minHeight: 40)
                        .background(Color("PrimaryColor")) // este viene de paleta de colores nuestra.
                })
                    .cornerRadius(15)
                    .padding(.top)
                    .padding(.horizontal)
                
                NavigationLink("Sign Up with Email", destination: SignUpView())
                    .padding(.vertical)
                
                HStack(alignment: .center) {
                    
                    Text("By proceeding, you also agree with our terms of service")
                        .multilineTextAlignment(.center)
                        .padding(.horizontal)
                    
                }
            }
            
        }
        .navigationBarTitle(Text("Sign in"))
        .navigationBarTitleDisplayMode(.inline)
        .background(
            Image("BackgroundImage")
                .resizable()
                .aspectRatio(2/3, contentMode: .fill)
                .frame(maxWidth: UIScreen.main.bounds.width, minHeight: UIScreen.main.bounds.height*0.8)
                .opacity(0.2)
        )
        
    }
}

//
//  ContentView.swift
//  WikiWiki
//
//  Created by Jorge Andrés Esguerra Alarcón on 9/21/21.
//

import SwiftUI
import FirebaseAuth

struct ContentView: View {
    @EnvironmentObject var viewModel: AuthViewModel
    @EnvironmentObject var orderViewModel: OrderViewModel
    
    var body: some View {
        NavigationView {
            if viewModel.isSignedIn {
                if viewModel.userProfile?.role == "OWNER" {
                    TabBarOwner()
                }
                else {
                    TabBar()
                }
            }
            else {
                SignInView()
            }
        }
        .ignoresSafeArea(.all)
        .edgesIgnoringSafeArea(.all)
        .onAppear {
            viewModel.signedIn = viewModel.isSignedIn
        }
    }
}



struct ContentView_Previews: PreviewProvider {
    static let viewModel = AuthViewModel()
    static let orderViewModel = OrderViewModel()
    static var previews: some View {
        ContentView()
            .environmentObject(viewModel)
            .environmentObject(orderViewModel)
    }
}

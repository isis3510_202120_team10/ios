//
//  TabBarOwner.swift
//  WikiWiki
//
//  Created by Jorge Andrés Esguerra Alarcón on 12/7/21.
//

import SwiftUI


struct TabBarOwner: View {
    
    var body: some View {
        TabView {
            CreateOrderView()
                .tabItem {
                    Label("Create Order", systemImage: "doc")
                }
            DriversList()
                .tabItem {
                    Label("Drivers", systemImage: "bicycle")
                }
            Profile()
                .tabItem {
                    Label("Profile", systemImage: "person")
                }
        }
        .accentColor(Color("PrimaryColor"))
    }
}

struct TabBarOwner_Previews: PreviewProvider {
    static let viewModel = AuthViewModel()
    static let orderViewModel = OrderViewModel()
    
    static var previews: some View {
        TabBar()
            .environmentObject(viewModel)
            .environmentObject(orderViewModel)
    }
}

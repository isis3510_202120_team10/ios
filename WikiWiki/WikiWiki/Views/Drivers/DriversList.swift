//
//  DriversList.swift
//  WikiWiki
//
//  Created by Carlos Infante on 7/12/21.
//

import Foundation
import SwiftUI
import UIKit

struct DriverCard: View {
    
    var status: Int
    var driver: String
    var time: String
    var textSize: CGFloat = 13
    var imageSize:CGFloat = 70
    @StateObject var delivery = DriverDeliveryData()
    
    
    var body: some View {
        VStack{
            HStack {
                Spacer()
                Image("apple")
                    .resizable()
                    .frame(width: imageSize, height: imageSize)
                    .clipShape(Circle())
                VStack(alignment: .leading, spacing: 10) {
                    Spacer()
                    HStack{
                        Image(systemName: "person")
                            .foregroundColor(Color("PrimaryColor"))
                        Text("Driver:")
                            .fontWeight(.bold)
                            .font(.system(size: textSize))
                            .foregroundColor(Color("PrimaryColor"))
                        Text(driver)
                            .font(.system(size: textSize))
                    }
                    HStack{
                        Image(systemName: "scooter")
                            .foregroundColor(Color("PrimaryColor"))
                        Text("Order status:")
                            .fontWeight(.bold)
                            .font(.system(size: textSize))
                            .foregroundColor(Color("PrimaryColor"))
                        Text(String(status))
                            .font(.system(size: textSize))
                    }
                    HStack{
                        Image(systemName: "clock")
                            .foregroundColor(Color("PrimaryColor"))
                        Text("Est. delivery:")
                            .fontWeight(.bold)
                            .font(.system(size: textSize))
                            .foregroundColor(Color("PrimaryColor"))
                        Text(time + " min")
                            .font(.system(size: textSize))
                    }
                    Spacer()
                }
                .padding(.trailing, 10)
                .frame(maxWidth: .infinity)
                
                Spacer()
            }
            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/) {
                Text("View Order")
            }
            .foregroundColor(.white)
            .padding(.all)
            .background(Color(red: 0.09803921568627451, green: 0.7019607843137254, blue: 0.4))
            .cornerRadius(20)
            Spacer()
        }
        .cornerRadius(10)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color(.sRGB, red: 150/255, green: 150/255, blue: 150/255, opacity: 0.3), lineWidth: 1)
        )
        .frame(maxWidth: .infinity)
        .edgesIgnoringSafeArea([.leading, .trailing])
    }
}


struct DriversList: View {
    @State private var searchText = ""
    @StateObject private var driversList = DriverDeliveryData()
    @State var hasNoInternet: Bool = !NetworkMonitor.shared.isConnected
    
    var body: some View {
        NavigationView {
            VStack {
                if hasNoInternet {
                    HStack {
                        // Banner Content
                        VStack(alignment: .leading, spacing: 2) {
                            Text("Connection error")
                                .bold()
                            Text("Network unavailable, please try again later")
                                .font(Font.system(size: 15, weight: Font.Weight.light, design: Font.Design.default))
                        }
                        .foregroundColor(.white)
                        Spacer()
                    }
                    
                    .padding(12)
                    .background(Color.red)
                    .cornerRadius(8)
                }
                List(searchResults, id: \.self) { delivery in
                    DriverCard(status: delivery.status, driver: delivery.driverName, time: String(delivery.duration))
                }
                .navigationTitle("Drivers")
                .listStyle(PlainListStyle())
            }
            
        }
        .navigationBarHidden(true)
        .accentColor(Color.white)
        .searchable(text: $searchText)
        .onChange(of: hasNoInternet) {[hasNoInternet] newState in
            if !hasNoInternet {
                driversList.fetchAllDrivers()
            }
        }
        .onAppear() {
            guard NetworkMonitor.shared.isConnected else {
                hasNoInternet = true
                driversList.load()
                return
            }
            
            if hasNoInternet {
                hasNoInternet = false
            }
            else {
                driversList.fetchAllDrivers()
            }
        }
        
    }
    
    var searchResults: [DriverDelivery] {
        if searchText.isEmpty {
            return driversList.driverDeliveries
        } else {
            return driversList.driverDeliveries.filter { $0.driverName.contains(searchText) }
        }
    }
}

struct DriversList_Previews: PreviewProvider {
    static var previews: some View {
        DriversList()
    }
}

//
//  SignUpView.swift
//  WikiWiki
//
//  Created by Jorge Andrés Esguerra Alarcón on 10/7/21.
//

import SwiftUI
import FirebaseAuth


struct SignUpView: View {
    @State var email = ""
    @State var password = ""
    @State var name = ""
    @State var lastName = ""
    @State var phoneNumber = ""
    @State var roleType = ""
    
    @State private var showingError = false
    
    @EnvironmentObject var viewModel: AuthViewModel
    @State private var sourceType: UIImagePickerController.SourceType?
    @State private var selectedImage: UIImage?
    @State private var isImagePickerDisplay = false
    @State private var showingAlert = false
    
    let defaultImage = UIImage(systemName: "person.circle")
    
    
    var body: some View {
        NavigationView {
            VStack {
                Image("WikiWikiLogo")
                    .resizable()
                    .scaledToFit()
                    .aspectRatio(2/3, contentMode: .fit)
                    .padding(.bottom)
                
                Button("Take Photo With Camera") {
                    self.isImagePickerDisplay.toggle()
                    self.sourceType = .camera
                }
                
                Button("Select Photo from Library") {
                    self.isImagePickerDisplay.toggle()
                    self.sourceType = .photoLibrary
                }
                
                VStack {
                    TextField("Email Address", text: $email)
                        .padding(10)
                        .overlay(RoundedRectangle(cornerRadius: 12.0).strokeBorder(Color("PrimaryColor"), style: StrokeStyle(lineWidth: 1.0)))
                    //.padding(10)
                        .disableAutocorrection(true)
                        .autocapitalization(.none)
                        .padding(.horizontal)
                    SecureField("Password", text: $password)
                        .padding(10)
                        .overlay(RoundedRectangle(cornerRadius: 12.0).strokeBorder(Color("PrimaryColor"), style: StrokeStyle(lineWidth: 1.0)))
                        .disableAutocorrection(true)
                        .autocapitalization(.none)
                        .padding(.horizontal)
                    TextField("Phone number", text: $phoneNumber)
                        .padding(10)
                        .overlay(RoundedRectangle(cornerRadius: 12.0).strokeBorder(Color("PrimaryColor"), style: StrokeStyle(lineWidth: 1.0)))
                        .disableAutocorrection(true)
                        .autocapitalization(.none)
                        .padding(.horizontal)
                    TextField("Name", text: $name)
                        .padding(10)
                        .overlay(RoundedRectangle(cornerRadius: 12.0).strokeBorder(Color("PrimaryColor"), style: StrokeStyle(lineWidth: 1.0)))
                        .disableAutocorrection(true)
                        .autocapitalization(.none)
                        .padding(.horizontal)
                    TextField("Last Name", text: $lastName)
                        .padding(10)
                        .overlay(RoundedRectangle(cornerRadius: 12.0).strokeBorder(Color("PrimaryColor"), style: StrokeStyle(lineWidth: 1.0)))
                        .disableAutocorrection(true)
                        .autocapitalization(.none)
                        .padding(.horizontal)
                    TextField("Role Type", text: $roleType)
                        .padding(10)
                        .overlay(RoundedRectangle(cornerRadius: 12.0).strokeBorder(Color("PrimaryColor"), style: StrokeStyle(lineWidth: 1.0)))
                        .disableAutocorrection(true)
                        .autocapitalization(.none)
                        .padding(.horizontal)
                    
                    
                    Button(action: {
                        guard allFieldsFilled(email: email,
                                              password: password,
                                              name: name,
                                              lastName: lastName,
                                              phoneNumber: phoneNumber,
                                              image: selectedImage)
                        else {
                            showingError = true
                            return
                        }
                        
                        guard NetworkMonitor.shared.isConnected else {
                            showingAlert = true
                            return
                        }
                        
                        viewModel.signUp(
                            email: email,
                            password: password,
                            name: name,
                            lastName: lastName,
                            phoneNumber: phoneNumber,
                            image: selectedImage!,
                            roleType: roleType
                        )
                    }, label: {
                        Text("Create Account")
                            .foregroundColor(Color.white)
                            .frame(maxWidth: UIScreen.main.bounds.width, minHeight: 40)
                        //.cornerRadius(10)
                            .background(Color.blue)
                    })
                        .alert("Network Unavailable, please try again later", isPresented: $showingAlert) {
                            Button("OK", role: .cancel) { }
                        }
                        .alert("Oops! Fill all fields and try again", isPresented: $showingError) {
                            Button("OK", role: .cancel) { }
                        }
                        .cornerRadius(15)
                        .padding()
                    
                    HStack(alignment: .center) {
                        
                        Text("By proceeding, you also agree with our terms of service")
                            .multilineTextAlignment(.center)
                            .padding(.horizontal)
                        
                    }
                }
                
            }
            .navigationBarTitle("Create Account")
            .navigationBarTitleDisplayMode(.inline)
            .sheet(isPresented: $isImagePickerDisplay)  {
                ImagePickerView(selectedImage: self.$selectedImage, sourceType: self.$sourceType)
            }
            Spacer()
        }
    }
}

struct SignUpView_Previews: PreviewProvider {
    static var previews: some View {
        SignUpView()
            .environmentObject(AuthViewModel())
    }
}

func allFieldsFilled(email: String,
                     password: String,
                     name: String,
                     lastName: String,
                     phoneNumber: String,
                     image: UIImage?) -> Bool {
    return !email.isEmpty && !password.isEmpty && !name.isEmpty && !lastName.isEmpty && !phoneNumber.isEmpty && image != nil
    
}

struct ImagePickerView: UIViewControllerRepresentable {
    
    @Binding var selectedImage: UIImage?
    @Environment(\.presentationMode) var isPresented
    @Binding var sourceType: UIImagePickerController.SourceType?
    
    
    func makeUIViewController(context: Context) -> UIImagePickerController {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = self.sourceType!
        imagePicker.delegate = context.coordinator // confirming the delegate
        return imagePicker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {
        
    }
    
    // Connecting the Coordinator class with this struct
    func makeCoordinator() -> Coordinator {
        return Coordinator(picker: self)
    }
    
    class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        var picker: ImagePickerView
        
        init(picker: ImagePickerView) {
            self.picker = picker
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            guard let selectedImage = info[.originalImage] as? UIImage else { return }
            self.picker.selectedImage = selectedImage
            self.picker.isPresented.wrappedValue.dismiss()
        }
        
    }
}

//
//  DeliveryInformation.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 8/10/21.
//

import SwiftUI

let NEXT_STATUS_NAME = ["Picked Up", "Delivered", "Finished"]

struct DeliveryInformation: View {
    var address: String
    var name: String
    var phone: String
    var status: Int
    
    var body: some View {
        GroupBox {
            VStack {
                HStack(spacing: 12.0) {
                    Image("WikiWikiLogo")
                        .resizable()
                        .frame(width: 80, height: 80)
                        .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                    VStack(alignment: .leading, spacing: 4.0) {
                        InformationField(label: "Address: ", value: address)
                        InformationField(label: "Name: ", value: name)
                        InformationField(label: "Phone: ", value: phone)
                    }
                }
                .padding(.bottom)
                HStack(spacing: 60) {
                    ButtonGroup(
                        name: "Directions",
                        firstIcon: "car",
                        secondIcon: "location",
                        firstAction: {},
                        secondAction: {}
                    )
                    ButtonGroup(
                        name: "Contact",
                        firstIcon: "message",
                        secondIcon: "phone",
                        firstAction: {},
                        secondAction: {}
                    )
                }
                Button(action: { }, label: {
                  Text("Mark as " + NEXT_STATUS_NAME[status])
                    .padding()
                    .foregroundColor(.white)
                    .frame(maxWidth: UIScreen.main.bounds.width*0.75)
                    .background(Color("PrimaryColor"))
                    .cornerRadius(12)
                })
            }
        }
        .frame(width: UIScreen.main.bounds.width*0.9)
        .groupBoxStyle(WhiteGroupBox())
    }
}

struct InformationField: View {
    var label: String
    var value: String
    
    var body: some View {
        HStack(spacing: 1.0) {
            Text(label).foregroundColor(Color("PrimaryColor"))
                .fontWeight(.bold)
            Text(value)
                .fontWeight(.regular)
        }.font(.body)
    }
}

struct ButtonGroup: View {
    var name: String
    var firstIcon: String
    var secondIcon: String
    var firstAction: () -> ()
    var secondAction: () -> ()
    
    var body: some View {
        VStack(spacing: 0) {
            Text(name).font(.headline)
            HStack(spacing: 20.0) {
                Button(action: firstAction) {
                    Label(name, systemImage: firstIcon)
                        .frame(width: 30.0, height: 30.0)
                        .labelStyle(.iconOnly)
                        .padding([.all], 8)
                        .foregroundColor(.white)
                        .background(Color("SecondaryColor"))
                        .cornerRadius(12)
                        .font(.title)
                }
                Button(action: secondAction) {
                    Label(name, systemImage: secondIcon)
                        .frame(width: 30.0, height: 30.0)
                        .labelStyle(.iconOnly)
                        .padding([.all], 8)
                        .foregroundColor(.white)
                        .background(Color("SecondaryColor"))
                        .cornerRadius(12)
                        .font(.title)
                }
            }
        }
    }
}

struct DeliveryInformation_Previews: PreviewProvider {
    static var previews: some View {
        DeliveryInformation(address: "address", name: "Name", phone: "2323", status: 0)
    }
}

//
//  MapView.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 8/10/21.
//

import SwiftUI
import MapKit
import FirebaseFirestore
import FirebaseFirestoreSwift

struct MapView: UIViewRepresentable {
    @Binding var hasNoInternet: Bool
    var destination: GeoPoint?
    var addressName: String?
    typealias UIViewType = MKMapView
    fileprivate let locationManager: CLLocationManager = CLLocationManager()
    var directionsTimer: Timer?
    
    func makeCoordinator() -> MapViewCoordinator {
        return MapViewCoordinator(self)
    }
    
    func makeUIView(context: Context) -> MKMapView {
        let mapView = MKMapView()
        mapView.delegate = context.coordinator
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        let lat = locationManager.location?.coordinate.latitude ?? 40.760596287711344
        let long = locationManager.location?.coordinate.longitude ?? -73.98224132946541
        let region = MKCoordinateRegion(
            center: CLLocationCoordinate2D(latitude: lat, longitude: long),
            span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        mapView.setRegion(region, animated: true)
        
        mapView.userTrackingMode = .followWithHeading
        mapView.showsUserLocation = true
        
        // Destination
        let p = MKPointAnnotation()
        p.coordinate = CLLocationCoordinate2D(latitude: destination?.latitude ?? 40.77383859884116, longitude: destination?.longitude ?? -73.9618701102291)
        p.title = addressName ?? ""
        mapView.addAnnotation(p)
        
        return mapView
    }
    
    func updateUIView(_ uiView: MKMapView, context: Context) {}
    
    class MapViewCoordinator: NSObject, MKMapViewDelegate {
        var parent: MapView
        
        init(_ parent: MapView){
            self.parent = parent
        }
        
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = .systemBlue
            renderer.lineWidth = 5
            return renderer
        }
        
        func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
            guard parent.directionsTimer == nil || parent.directionsTimer!.fireDate < Date() else {
                return
            }
            
            guard NetworkMonitor.shared.isConnected else {
                parent.hasNoInternet = true
                return
            }
            parent.hasNoInternet = false
            
            parent.directionsTimer = Timer.scheduledTimer(withTimeInterval: 20, repeats: false, block: { (timer) in})
            
            // Destination
            let p2 = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: parent.destination?.latitude ?? 40.77383859884116, longitude: parent.destination?.longitude ?? -73.9618701102291))
            
            let request = MKDirections.Request()
            request.source = MKMapItem.forCurrentLocation()
            request.destination = MKMapItem(placemark: p2)
            request.transportType = .automobile
            
            let directions = MKDirections(request: request)
            directions.calculate { response, error in
                guard let route = response?.routes.first else { return }
                mapView.removeOverlays(mapView.overlays)
                mapView.addOverlay(route.polyline)
                mapView.setVisibleMapRect(
                    route.polyline.boundingMapRect,
                    edgePadding: UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20),
                    animated: true)
            }
        }
    }
}

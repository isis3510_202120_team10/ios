//
//  CurrentDeliveryStatus.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 8/10/21.
//

import SwiftUI

let STATUS_NAME = ["Picking Up", "Delivering", "Finished"]
let STATUS_COLOR = [Color.orange, Color.blue, Color.pink]

struct CurrentDeliveryStatus: View {
    var status: Int
    
    var body: some View {
        GroupBox {
            VStack {
                HStack {
                    Text("Delivery status: ")
                    Text(STATUS_NAME[status]).foregroundColor(STATUS_COLOR[status])
                }.font(.system(size: 16, weight: .heavy, design: .default))
                HStack {
                    Status(name: "Picking Up", color: Color.orange)
                    Status(name: "Delivering", color: Color.blue)
                    Status(name: "Finished", color: Color.pink)
                }
            }
        }
        .groupBoxStyle(WhiteGroupBox())
    }
}

struct Status: View {
    var name: String
    var color: Color
    
    var body: some View {
        VStack{
            Circle()
                .strokeBorder(color,lineWidth: 4)
                .background(Circle().foregroundColor(Color.white))
                .frame(width: 16, height: 16)
            Text(name).font(Font.caption).foregroundColor(Color.gray)
        }
    }
}

struct WhiteGroupBox: GroupBoxStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.content
            .padding()
            .background(RoundedRectangle(cornerRadius: 12).fill(Color.white))
    }
}

struct CurrentDeliveryStatus_Previews: PreviewProvider {
    static var previews: some View {
        CurrentDeliveryStatus(status: 1)
    }
}

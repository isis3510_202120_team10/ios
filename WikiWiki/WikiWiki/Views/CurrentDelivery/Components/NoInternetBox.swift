//
//  NoInternetBow.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 17/11/21.
//

import SwiftUI

struct NoInternetBox: View {
    @Binding var hasNoInternet: Bool
    
    var body: some View {
        GroupBox(label: Label("No Internet Connection", systemImage: "wifi.slash").foregroundColor(.red)) {
            HStack {
                Text("The route and delivery information will not update until you reconnect.").padding()
                Button(action: {
                    hasNoInternet = !NetworkMonitor.shared.isConnected
                }) {
                    Label("Refresh Internet", systemImage: "goforward")
                        .frame(width: 30.0, height: 30.0)
                        .labelStyle(.iconOnly)
                        .padding([.all], 8)
                        .foregroundColor(.blue)
                        .font(.title)
                }
            }
        }
        .padding()
    }
}

struct NoInternetBox_Previews: PreviewProvider {
    static var previews: some View {
        NoInternetBox(hasNoInternet: .constant(true))
    }
}

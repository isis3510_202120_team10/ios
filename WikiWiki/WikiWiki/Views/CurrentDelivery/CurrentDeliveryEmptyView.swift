
//
//  CurrentDeliveryEmptyView.swift
//  feature-view
//
//  Created by Carlos Infante on 8/10/21.
//

import SwiftUI

struct CurrentDeliveryEmptyView: View {
    @StateObject var interval = Api()
    @State var hasNoInternet: Bool = !NetworkMonitor.shared.isConnected
    
    var body: some View {
        VStack(alignment: .center){
            Image("WaitImage")
                .resizable()
                .aspectRatio(contentMode: .fit)
            VStack(spacing: 20){
                if hasNoInternet {
                        HStack {
                            // Banner Content
                            VStack(alignment: .leading, spacing: 2) {
                                Text("Connection error")
                                    .bold()
                                Text("Network unavailable, please try again later")
                                    .font(Font.system(size: 15, weight: Font.Weight.light, design: Font.Design.default))
                            }
                            .foregroundColor(.white)
                            Spacer()
                        }
                    .padding(12)
                    .background(Color.black)
                    .cornerRadius(8)
                }
                Text("You don't have any deliveries in progress")
                    .multilineTextAlignment(.center)
                Text("You can sit back, relax and enjoy life.")
                .onAppear() {
                    interval.loadData()
                }
                if interval.time > 0{
                    let roundedValue = String(format: "%.2f", interval.time)
                    Text("The average waiting time is \(roundedValue) mins")
                    .multilineTextAlignment(.center)
                }
                else {
                        Text("There is no sufficient data to provide an estimated time")
                            .multilineTextAlignment(.center)
                }
                Text("We'll notify you when a new delivery is assigned to you")
                    .multilineTextAlignment(.center)
                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/) {
                    Text("See previous deliveries")
                }
                .foregroundColor(.white)
                        .padding(.all)
                        .background(Color(red: 0.09803921568627451, green: 0.7019607843137254, blue: 0.4))
                        .cornerRadius(20)
            }
            .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
            Spacer()
        }
        .onAppear() {
            guard NetworkMonitor.shared.isConnected else {
                hasNoInternet = true
                return
            }
            
            if hasNoInternet {
                hasNoInternet = false
            }
        }
    }
}

struct CurrentDeliveryEmptyView_Previews: PreviewProvider {
    static var previews: some View {
        CurrentDeliveryEmptyView()
    }
}

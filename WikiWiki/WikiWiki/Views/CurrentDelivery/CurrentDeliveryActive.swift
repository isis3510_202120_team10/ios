//
//  CurrentDeliveryActive.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 8/10/21.
//

import SwiftUI
import MapKit

struct CurrentDeliveryActive: View {
    @StateObject var delivery = CurrentDeliveryData()
    @State var navBarHidden: Bool = true
    @State var hasNoInternet: Bool = !NetworkMonitor.shared.isConnected

    var body: some View {
        ZStack {
            MapView(hasNoInternet: $hasNoInternet, destination: delivery.delivery?.deliveryOrigin.coordinate, addressName: delivery.delivery?.deliveryOrigin.address)
            VStack {
                if hasNoInternet {
                    NoInternetBox(hasNoInternet: $hasNoInternet)
                        .offset(y: 50)
                }
                else {
                    CurrentDeliveryStatus(status: delivery.delivery?.status ?? 0)
                        .offset(y: 50)
                }
                Spacer()
                DeliveryInformation(address: delivery.delivery?.deliveryOrigin.address ?? "", name: delivery.delivery?.clientName ?? "", phone: delivery.delivery?.phone ?? "", status: delivery.delivery?.status ?? 0)
                    .offset(y: -30)
            }
        }
        .edgesIgnoringSafeArea([.top])
        .navigationBarTitle("")
        .navigationBarHidden(self.navBarHidden)
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification)) { _ in
            self.navBarHidden = true
        }
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.willResignActiveNotification)) { _ in
            self.navBarHidden = false
        }
        .onChange(of: hasNoInternet) {[hasNoInternet] newState in
            if !hasNoInternet {
                delivery.loadDelivery()
            }
        }
        .onAppear {
            self.navBarHidden = true
            
            guard NetworkMonitor.shared.isConnected else {
                hasNoInternet = true
                delivery.loadCachedDelivery()
                return
            }
            
            if hasNoInternet {
                hasNoInternet = false
            }
            else {
                delivery.loadDelivery()
            }
        }
        .onDisappear {
            self.navBarHidden = false
        }
    }
}

struct CurrentDeliveryActive_Previews: PreviewProvider {
    static let viewModel = AuthViewModel()
    
    static var previews: some View {
        CurrentDeliveryActive()
            .environmentObject(viewModel)
    }
}

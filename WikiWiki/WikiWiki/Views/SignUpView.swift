//
//  SignUpView.swift
//  WikiWiki
//
//  Created by Jorge Andrés Esguerra Alarcón on 10/7/21.
//

import SwiftUI
import FirebaseAuth


struct SignUpView: View {
    @State var email = ""
    @State var password = ""
    
    @EnvironmentObject var viewModel: AppViewModel
    var body: some View {
        
        VStack {
            Image("WikiWikiLogo")
                .resizable()
                .scaledToFit()
                .aspectRatio(2/3, contentMode: .fit)
                .padding(.bottom)
            
            VStack {
                TextField("Email Address", text: $email)
                    .padding(10)
                    .overlay(RoundedRectangle(cornerRadius: 12.0).strokeBorder(Color("PrimaryColor"), style: StrokeStyle(lineWidth: 1.0)))
                    //.padding(10)
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
                    .padding(.horizontal)
                
                
                SecureField("Password", text: $password)
                    .padding(10)
                    .overlay(RoundedRectangle(cornerRadius: 12.0).strokeBorder(Color("PrimaryColor"), style: StrokeStyle(lineWidth: 1.0)))
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
                    .padding(.horizontal)
                
                Button(action: {
                    guard !email.isEmpty, !password.isEmpty else {
                        return
                    }
                    viewModel.signUp(email: email, password: password)
                }, label: {
                    Text("Create Account")
                        .foregroundColor(Color.white)
                        .frame(maxWidth: UIScreen.main.bounds.width, minHeight: 40)
                    //.cornerRadius(10)
                        .background(Color.blue)
                })
                    .cornerRadius(15)
                    .padding()
              
// for crash reporting purposes, delete soon.
//                Button(action: {
//                    viewModel.crashApp()
//                }, label: {
//                    Text("Test Button To crash")
//                        .foregroundColor(Color.red)
//                        .frame(maxWidth: UIScreen.main.bounds.width, minHeight: 40)
//                    //.cornerRadius(10)
//                        .background(Color.blue)
//                })
//                    .cornerRadius(15)
//                    .padding()
                
                HStack(alignment: .center) {
                    
                    Text("By proceeding, you also agree with our terms of service")
                        .multilineTextAlignment(.center)
                        .padding(.horizontal)
                    
                }
                //Text("By proceeding, you also agree with our terms of service")
            }
            
        } .navigationTitle("Create Account")
            .navigationBarTitleDisplayMode(.inline)
            .padding()
        Spacer()
    }
}

//
//  DeliveryStatusRowView.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 7/12/21.
//

import SwiftUI

struct DeliveryStatusRowView: View {
    var body: some View {
        HStack(alignment: .top, spacing: 28) {
            VStack {
                Text("Restaurant").foregroundColor(Color("PrimaryColor"))
                    .fontWeight(.bold)
                Image("WikiWikiLogo")
                    .resizable()
                    .frame(width: 80, height: 80)
                    .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            }
            VStack {
                Text("Driver").foregroundColor(Color("PrimaryColor"))
                    .fontWeight(.bold)
                Image("WikiWikiLogo")
                    .resizable()
                    .frame(width: 80, height: 80)
                    .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            }
            VStack() {
                Text("Order Status").foregroundColor(Color("PrimaryColor"))
                    .fontWeight(.bold)
                Status(name: STATUS_NAME[1], color: STATUS_COLOR[1])
                    .padding([.top], 20)
            }
        }
    }
}

struct DeliveryStatusRowView_Previews: PreviewProvider {
    static var previews: some View {
        DeliveryStatusRowView()
    }
}

//
//  DeliveryBasicsRowView.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 7/12/21.
//

import SwiftUI

struct DeliveryBasicsRowView: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 12) {
            HStack(spacing: 4) {
                Text("Order ID: ").foregroundColor(Color("PrimaryColor"))
                    .fontWeight(.bold)
                    .frame(width: 100, alignment: .leading)
                Text("SBA49")
                    .fontWeight(.regular)
            }.font(.body)
            HStack(spacing: 4) {
                Text("Created At: ").foregroundColor(Color("PrimaryColor"))
                    .fontWeight(.bold)
                    .frame(width: 100, alignment: .leading)
                Text("09-Sep-2021 16:32:12")
                    .fontWeight(.regular)
            }.font(.body)
            HStack(spacing: 4) {
                Text("Time Spent: ").foregroundColor(Color("PrimaryColor"))
                    .fontWeight(.bold)
                    .frame(width: 100, alignment: .leading)
                Text("40 min")
                    .fontWeight(.regular)
            }.font(.body)
        }
    }
}

struct DeliveryBasicsRowView_Previews: PreviewProvider {
    static var previews: some View {
        DeliveryBasicsRowView()
    }
}

//
//  DeliveryDetailView.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 7/12/21.
//

import SwiftUI
import FirebaseFirestore
import FirebaseFirestoreSwift

struct DeliveryDetailView: View {
    var orderID: String
    @StateObject var delivery = DeliveryDetailData()
    @State var hasNoInternet: Bool = !NetworkMonitor.shared.isConnected
    
    var body: some View {
        GeometryReader { metrics in
            VStack(spacing: 0) {
                RouteMapView(hasNoInternet: $hasNoInternet, origin: delivery.delivery?.deliveryOrigin.coordinate, destination: delivery.delivery?.deliveryDestination.coordinate)
                VStack(alignment: .leading, spacing: 0) {
                    DeliveryStatusRowView(status: delivery.delivery?.status ?? 1)
                        .frame(width: metrics.size.width, height: metrics.size.height*0.2)
                    Divider()
                    DeliveryBasicsRowView(
                        orderID: orderID,
                        createdAt: delivery.formatDate(date: delivery.delivery?.startDate.dateValue()),
                        duration: delivery.getDifferenceInMinutes(start: delivery.delivery?.startDate.dateValue(), end: delivery.delivery?.finishDate.dateValue())
                    )
                        .frame(height: metrics.size.height*0.15)
                        .padding()
                    Divider()
                    DeliveryLocationRowView(deliveryOrigin: delivery.delivery?.deliveryOrigin.address, deliveryDestination: delivery.delivery?.deliveryDestination.address)
                        .frame(height: metrics.size.height*0.08)
                        .padding()
                    Divider()
                    DeliveryImageRowView()
                        .frame(height: metrics.size.height*0.12)
                        .padding()
                }
            }
            .alert(isPresented: $delivery.notFound) {
                Alert(title: Text("No internet connection"), message: Text("Please check your internet connection to view the order detail."), dismissButton: .default(Text("Got it!")))
            }
        }
        .navigationTitle("Delivery Detail")
        .onChange(of: hasNoInternet) {[hasNoInternet] newState in
            if !hasNoInternet {
                delivery.loadDelivery(orderId: orderID)
            }
        }
        .onAppear {
            guard NetworkMonitor.shared.isConnected else {
                hasNoInternet = true
                delivery.loadCachedDelivery(orderId: orderID)
                return
            }
            
            if hasNoInternet {
                hasNoInternet = false
            }
            else {
                delivery.loadDelivery(orderId: orderID)
            }
        }
    }
}

struct DeliveryDetailView_Previews: PreviewProvider {
    static let viewModel = AuthViewModel()
    
    static var previews: some View {
        DeliveryDetailView(orderID: "ABCDEF")
            .environmentObject(viewModel)
    }
}

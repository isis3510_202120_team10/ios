//
//  DeliveryStatusRowView.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 7/12/21.
//

import SwiftUI

struct DeliveryStatusRowView: View {
    var status: Int
    @EnvironmentObject var viewModel: AuthViewModel
    
    var body: some View {
        HStack(alignment: .top, spacing: 28) {
            VStack {
                Text("Restaurant").foregroundColor(Color("PrimaryColor"))
                    .fontWeight(.bold)
                Image("WikiWikiLogo")
                    .resizable()
                    .frame(width: 80, height: 80)
                    .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            }
            VStack {
                Text("Driver").foregroundColor(Color("PrimaryColor"))
                    .fontWeight(.bold)
                AsyncImage(
                    url: URL(string: viewModel.userProfile?.photoUrl ?? viewModel.cache.object(forKey: "userProfile")?.photoUrl ?? "https://via.placeholder.com/80x80"),
                    content: { image in
                        image.resizable()
                        .frame(width: 80, height: 80)
                        .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                    },
                    placeholder: {
                        ProgressView()
                    }
                )
                    .frame(maxWidth: 80, maxHeight: 80)
            }
            VStack() {
                Text("Order Status").foregroundColor(Color("PrimaryColor"))
                    .fontWeight(.bold)
                Status(name: STATUS_NAME[status], color: STATUS_COLOR[status])
                    .padding([.top], 20)
            }
        }
    }
}

struct DeliveryStatusRowView_Previews: PreviewProvider {
    static var previews: some View {
        DeliveryStatusRowView(status: 1)
    }
}

//
//  DeliveryImageRowView.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 7/12/21.
//

import SwiftUI

struct DeliveryImageRowView: View {
    var body: some View {
        HStack {
            Text("Proof of delivery Image").foregroundColor(Color("PrimaryColor"))
                .fontWeight(.bold)
                .font(.headline)
                .padding([.trailing])
            Image("placeholder")
                .resizable()
                .frame(width: 80, height: 80)
        }
    }
}

struct DeliveryImageRowView_Previews: PreviewProvider {
    static var previews: some View {
        DeliveryImageRowView()
    }
}

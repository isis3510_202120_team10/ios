//
//  DeliveryBasicsRowView.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 7/12/21.
//

import SwiftUI
import FirebaseFirestore
import FirebaseFirestoreSwift

struct DeliveryBasicsRowView: View {
    var orderID: String
    var createdAt: String
    var duration: Int?
    
    var body: some View {
        VStack(alignment: .leading, spacing: 12) {
            HStack(spacing: 4) {
                Text("Order ID: ").foregroundColor(Color("PrimaryColor"))
                    .fontWeight(.bold)
                    .frame(width: 100, alignment: .leading)
                Text(orderID)
                    .fontWeight(.regular)
            }.font(.body)
            HStack(spacing: 4) {
                Text("Created At: ").foregroundColor(Color("PrimaryColor"))
                    .fontWeight(.bold)
                    .frame(width: 100, alignment: .leading)
                Text(createdAt)
                    .fontWeight(.regular)
            }.font(.body)
            HStack(spacing: 4) {
                Text("Time Spent: ").foregroundColor(Color("PrimaryColor"))
                    .fontWeight(.bold)
                    .frame(width: 100, alignment: .leading)
                Text("" + String(duration ?? 0) + " min")
                    .fontWeight(.regular)
            }.font(.body)
        }
    }
}

struct DeliveryBasicsRowView_Previews: PreviewProvider {
    static var previews: some View {
        DeliveryBasicsRowView(orderID: "ABCDEF", createdAt: "Yesterday", duration: 30)
    }
}

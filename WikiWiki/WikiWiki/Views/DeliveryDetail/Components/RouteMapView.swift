//
//  RouteMapView.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 8/10/21.
//

import SwiftUI
import MapKit
import FirebaseFirestore
import FirebaseFirestoreSwift

struct RouteMapView: UIViewRepresentable {
    @Binding var hasNoInternet: Bool
    var origin: GeoPoint?
    var destination: GeoPoint?
    typealias UIViewType = MKMapView
    
    func makeCoordinator() -> RouteMapViewCoordinator {
        return RouteMapViewCoordinator(self)
    }
    
    func makeUIView(context: Context) -> MKMapView {
        let mapView = MKMapView()
        mapView.delegate = context.coordinator
        // mapView.addConstraint(height)
        
        let region = MKCoordinateRegion(
            center: CLLocationCoordinate2D(latitude: origin?.latitude ?? 40.77383859884116, longitude: origin?.latitude ?? -73.9618701102291),
            span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        mapView.setRegion(region, animated: false)
        
        // Origin
        let p1 = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: origin?.latitude ?? 40.760596287711344, longitude: origin?.longitude ?? -73.98224132946541))
        mapView.addAnnotation(p1)
        print(p1.coordinate)
        
        // Destination
        let p2 = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: destination?.latitude ?? 40.77383859884116, longitude: destination?.longitude ?? -73.9618701102291))
        mapView.addAnnotation(p2)
        print(p2.coordinate)
        
        return mapView
    }
    
    func updateUIView(_ mapView: MKMapView, context: Context) {
        mapView.removeAnnotations(mapView.annotations)
        
        // Origin
        let p1 = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: origin?.latitude ?? 40.760596287711344, longitude: origin?.longitude ?? -73.98224132946541))
        mapView.addAnnotation(p1)
        
        // Destination
        let p2 = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: destination?.latitude ?? 40.77383859884116, longitude: destination?.longitude ?? -73.9618701102291))
        mapView.addAnnotation(p2)
        
        mapView.showAnnotations(mapView.annotations, animated: true)
        
        guard NetworkMonitor.shared.isConnected else {
            hasNoInternet = true
            return
        }
        
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: p1)
        request.destination = MKMapItem(placemark: p2)
        request.transportType = .automobile
        
        let directions = MKDirections(request: request)
        directions.calculate { response, error in
            guard let route = response?.routes.first else { return }
            mapView.removeOverlays(mapView.overlays)
            mapView.addOverlay(route.polyline)
        }
    }
    
    class RouteMapViewCoordinator: NSObject, MKMapViewDelegate {
        var parent: RouteMapView
        
        init(_ parent: RouteMapView){
            self.parent = parent
        }
        
        func RouteMapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = .systemBlue
            renderer.lineWidth = 5
            return renderer
        }
    }
}

//
//  DeliveryLocationRowView.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 7/12/21.
//

import SwiftUI

struct DeliveryLocationRowView: View {
    var deliveryOrigin: String?
    var deliveryDestination: String?
    
    var body: some View {
        VStack(alignment: .leading, spacing: 12) {
            HStack(spacing: 4) {
                Text("Delivery Origin: ").foregroundColor(Color("PrimaryColor"))
                    .fontWeight(.bold)
                    .frame(width: 180, alignment: .leading)
                Text(deliveryOrigin ?? "")
                    .fontWeight(.regular)
            }.font(.body)
            HStack(spacing: 4) {
                Text("Delivery Destination: ").foregroundColor(Color("PrimaryColor"))
                    .fontWeight(.bold)
                    .frame(width: 180, alignment: .leading)
                Text(deliveryDestination ?? "")
                    .fontWeight(.regular)
            }.font(.body)
        }
    }
}

struct DeliveryLocationRowView_Previews: PreviewProvider {
    static var previews: some View {
        DeliveryLocationRowView(deliveryOrigin: "Cra 4 # 87 - 93", deliveryDestination: "Cra 14 # 94a - 61")
    }
}

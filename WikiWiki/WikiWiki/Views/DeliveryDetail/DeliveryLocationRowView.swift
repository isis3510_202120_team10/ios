//
//  DeliveryLocationRowView.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 7/12/21.
//

import SwiftUI

struct DeliveryLocationRowView: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 12) {
            HStack(spacing: 4) {
                Text("Delivery Origin: ").foregroundColor(Color("PrimaryColor"))
                    .fontWeight(.bold)
                    .frame(width: 180, alignment: .leading)
                Text("Cra 4 # 87 - 93")
                    .fontWeight(.regular)
            }.font(.body)
            HStack(spacing: 4) {
                Text("Delivery Destination: ").foregroundColor(Color("PrimaryColor"))
                    .fontWeight(.bold)
                    .frame(width: 180, alignment: .leading)
                Text("Cra 14 # 94a - 61")
                    .fontWeight(.regular)
            }.font(.body)
        }
    }
}

struct DeliveryLocationRowView_Previews: PreviewProvider {
    static var previews: some View {
        DeliveryLocationRowView()
    }
}

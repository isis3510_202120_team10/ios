//
//  DeliveryWaitingTime.swift
//  feature-view
//
//  Created by Carlos Infante on 8/10/21.
//

import Foundation

class Api: ObservableObject{
    
    @Published var time = 0.0
    
    func loadData() {
        let userId = "Gow1nOcc76VAsKSB2xegXJuFmi72"
        guard let url = URL(string: "https://us-central1-wikiwiki-moviles2021-20.cloudfunctions.net/averageTimeDelivery/?user=\(userId)") else {
            print("Invalid url...")
            return
        }
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            let response = String(data: data, encoding: .utf8)!
            self.time = (response as NSString).doubleValue
        }

        task.resume()
        
    }
}

//
//  DriverDeliveryData.swift
//  WikiWiki
//
//  Created by Carlos Infante on 8/12/21.
//

import Foundation
import Firebase

final class DriverDeliveryData: ObservableObject {
    let db = Firestore.firestore()
    @Published var driverDeliveries: [DriverDelivery] = []
    
    private static var documentsFolder: URL {
        do {
            return try FileManager.default.url(for: .documentDirectory,
                                                  in: .userDomainMask,
                                                  appropriateFor: nil,
                                                  create: false)
        } catch {
            fatalError("Can't find documents directory.")
        }
    }
    
    private static var fileURL: URL {
        return documentsFolder.appendingPathComponent("driverDelivery.data")
    }
    
    func fetchAllDrivers() {
        db.collection("delivery").addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                print("No documents")
                return
            }
            self.driverDeliveries = documents.map { queryDocumentSnapshot -> DriverDelivery in
                let data = queryDocumentSnapshot.data()
                
                let clientName = data["clientName"] as? String ?? ""
                let status = data["status"] as? Int ?? 0
                let deliveryId = data["orderID"] as? String ?? ""
                let startDate = (data["startDate"] as? Timestamp)?.dateValue() ?? Date()
                let finishDate = (data["finishDate"] as? Timestamp)?.dateValue() ?? Date()
                let diffs = Calendar.current.dateComponents([.hour, .minute], from: startDate, to: finishDate)
                let duration = (diffs.hour! * 60) + diffs.minute!
                
                return DriverDelivery(id: .init(),driverName: clientName, duration: duration, orderID: deliveryId, status: status)
            }
            self.save()
        }
    }
    
    func load() {
        DispatchQueue.global(qos: .background).async{ [weak self] in
            guard let data = try? Data(contentsOf: Self.fileURL) else {
                return
            }
            guard let dailyDriverDeliveries = try? JSONDecoder().decode([DriverDelivery].self, from: data) else {
                fatalError("Can't decode saved driver deliveries data.")
            }
            DispatchQueue.main.async {
                self?.driverDeliveries = dailyDriverDeliveries
            }
        }
    }
    
    func save() {
            DispatchQueue.global(qos: .background).async { [weak self] in
                guard let driverDeliveries = self?.driverDeliveries else { fatalError("Self out of scope") }
                guard let data = try? JSONEncoder().encode(driverDeliveries) else { fatalError("Error encoding data") }
                do {
                    let outfile = Self.fileURL
                    try data.write(to: outfile)
                } catch {
                    fatalError("Can't write to file")
                }
            }
        }
    
}

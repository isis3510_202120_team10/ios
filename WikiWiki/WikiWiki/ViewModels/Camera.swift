//
//  Camera.swift
//  WikiWiki
//
//  Created by Jorge Andrés Esguerra Alarcón on 10/29/21.
//
// MVC class!
// Controller is a bundle of views. (not the same as MVVM views)

import SwiftUI

struct Camera: UIViewControllerRepresentable {
    
    var handlePickedImage: (UIImage?) -> Void // this is a closure
    
    static var isAvailable: Bool {
        UIImagePickerController.isSourceTypeAvailable(.camera)
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(handlePickedImage: handlePickedImage)
    }
    
    // create
    func makeUIViewController(context: Context) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.sourceType = .camera
        picker.allowsEditing = true
        picker.delegate = context.coordinator // created with makeCoordinator up there.
        return picker
    }
    
    // update
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {
        // nothing to do
    }
    
    class Coordinator: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        var handlePickedImage: (UIImage?) -> Void // this is a closure
        
        init(handlePickedImage: @escaping (UIImage?) -> Void) {
            self.handlePickedImage = handlePickedImage
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            handlePickedImage(nil)
        }
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            handlePickedImage((info[.editedImage] ?? info[.originalImage]) as? UIImage)
        }
        
    }
}

//
//  AuthViewModel.swift
//  WikiWiki
//
//  Created by Jorge Andrés Esguerra Alarcón on 10/7/21.
//

import SwiftUI
import FirebaseAuth
import FirebaseFirestore
import FirebaseCore
import FirebaseFirestoreSwift
import FirebaseStorage

// Este se encarga de hacer los requests!
// Puede crearse otra capa por encima del ViewModel. Ejemplo un servicio
// Auth manager que se encarga de hacer los requests y retorna los resultados al ViewModel.


class AuthViewModel: ObservableObject {
    let auth = Auth.auth()
    private let database = Firestore.firestore()
    private let storageRef = Storage.storage().reference() // singleton
    @Published var signedIn = false
    @Published var userProfile: UserProfile? = nil // will be created / assigned upon login.
    

    @Published var cache = Cache<String, UserProfile>()
    
    var isSignedIn: Bool {
        return auth.currentUser != nil
    }
    
    func onSignInCompletion() {
        //print("function called")
        let uid = auth.currentUser?.uid
        guard uid != nil else {
            return
        }
        let docRef = database.collection("user").document(uid!)
        
        docRef.getDocument { (document, error) in
            let result = Result {
                try document?.data(as: UserProfile.self)
            }
            switch result {
            case .success(let userProfile):
                if let userProfile = userProfile {
                    // A `UserProfile` value was successfully initialized from the DocumentSnapshot.
                    print("userProfile: \(userProfile)")
                    self.userProfile = userProfile
                    self.cache.setObject(userProfile, forKey: "userProfile")
                } else {
                    // A nil value was successfully initialized from the DocumentSnapshot,
                    // or the DocumentSnapshot was nil.
                    print("User Document does not exist")
                }
            case .failure(let error):
                // A `City` value could not be initialized from the DocumentSnapshot.
                print("Error decoding userProfile: \(error)")
            }
        }
        
    }
    
    func signIn(email: String, password: String) {
        auth.signIn(withEmail: email, password: password) { [weak self] (result, error) in
            guard result != nil, error == nil else {
                return
            }
            // Esto ya lo valdrían como threadding.
            DispatchQueue.main.async {
                self?.onSignInCompletion()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self?.signedIn = true
            }
        }
    }
    
    func onSignUpCompletion(name: String, lastName: String, phoneNumber: String, photo: UIImage, role: String) {
        let userUid = auth.currentUser?.uid
        print("este es: \(userUid!)") // TODO: DEBUG
        guard userUid != nil else {
            print("no cree el documento en la db:v") // TODO: DEBUG remove
            return
        }
        let path = "\(userUid!)/profile/userImage.png"
        let riversRef = storageRef.child(path)
        
        // Upload the file to the path "images/rivers.jpg"
        let uploadTask = riversRef.putData(photo.pngData()!, metadata: nil) { (metadata, error) in
            
            riversRef.downloadURL(completion: { url, error in
                guard let url = url, error == nil else {
                    print ("error de url o de otra cosa :S")
                    return
                }
                
                let urlString = url.absoluteString
                print("Download URL: \(urlString)")
                
                let docData: [String: Any] = [
                    "name": name,
                    "lastName": lastName,
                    "id": userUid!,
                    "phoneNumber": phoneNumber,
                    "role": role, // TODO: update when roles are defined!
                    "photo": urlString
                ]
                print("creacion de docData completada")
                
                let userDocRef = self.database.document("user/\(userUid!)")
                userDocRef.setData(docData)
                print("Hice setData del nuevo usuario")
                
            })
        }
        self.onSignInCompletion()
    }
    
    func signUp(email: String, password: String, name: String, lastName: String, phoneNumber: String, image: UIImage, roleType: String) {
        auth.createUser(withEmail: email, password: password) { [weak self] result, error in
            guard result != nil, error == nil else {
                return
            }
            
            DispatchQueue.main.async {
                self?.onSignUpCompletion(name: name, lastName: lastName, phoneNumber: phoneNumber, photo: image, role: roleType)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self?.signedIn = true
            }
        }
    }
    
    func signOut() {
        try? auth.signOut()
        self.userProfile = nil
        self.signedIn = false
    }
    
    // for crash testing purposes.
    //    @IBAction func crashApp() {
    //        let numbers = [0]
    //        let _ = numbers[1]
    //    }
}

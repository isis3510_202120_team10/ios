//
//  AuthViewModel.swift
//  WikiWiki
//
//  Created by Jorge Andrés Esguerra Alarcón on 10/7/21.
//

import SwiftUI
import FirebaseAuth
import FirebaseFirestore
import FirebaseCore
import FirebaseFirestoreSwift
import FirebaseStorage

// Este se encarga de hacer los requests!
// Puede crearse otra capa por encima del ViewModel. Ejemplo un servicio
// Auth manager que se encarga de hacer los requests y retorna los resultados al ViewModel.


class OrderViewModel: ObservableObject {
    private let database = Firestore.firestore()
    private let storageRef = Storage.storage().reference() // singleton
    
    func createOrder(clientName: String, phoneNumber: String, destinationAddress: String, product: String, payOnDelivery: Bool, deliveryPrice: String) {
        let orderID = UUID().uuidString
        let orderData: [String: Any] = [
            "clientName": clientName,
            "deliveryOrigin": ["address": destinationAddress, "coordinate": GeoPoint(latitude: 4.704282, longitude: -74.058431)],
            "deliveryDestination": ["address": " Cl. 93 ##12-41, Bogotá ", "coordinate": GeoPoint(latitude: 4.673454, longitude: -74.043767)],
            "status": 0,
            "orderID": orderID,
            "phone": phoneNumber,
            "product": product,
            "willPayOnDelivery": payOnDelivery,
            "deliveryPrice": deliveryPrice
        ]
        print("creacion de orderData completada ")
        print(orderData)
        
        let userDocRef = self.database.document("delivery/\(orderID)")
        userDocRef.setData(orderData)
        print("Hice setData de la nueva orden")
    }
}


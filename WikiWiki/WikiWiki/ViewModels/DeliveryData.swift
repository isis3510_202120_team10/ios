//
//  DeliveryData.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 9/10/21.
//

import Foundation
import Firebase

final class CurrentDeliveryData: ObservableObject {
    let db = Firestore.firestore()
    @Published var delivery: Delivery?
    
    private static var documentsFolder: URL {
        do {
            return try FileManager.default.url(for: .documentDirectory,
                                               in: .userDomainMask,
                                               appropriateFor: nil,
                                               create: false)
        } catch {
            fatalError("Can't find documents directory.")
        }
    }
    
    private static var fileURL: URL {
        return documentsFolder.appendingPathComponent("currentDelivery.data")
    }
    

    func loadDelivery() {
        db.collection("delivery").document("Udapb02ELTkvwpNdhrH9").getDocument { (document, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            }
        
            if let doc = document {
                do {
                    self.delivery = try doc.data(as: Delivery.self)
                    self.saveToCache()
                } catch {
                    print("Error Mapping Document: \(error)")
                }
            }
            else {
                print("Error. no document")
            }
        }
    }
    
    func saveToCache() {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let currentDelivery = self?.delivery else { fatalError("Self out of scope") }
            guard let data = try? JSONEncoder().encode(currentDelivery) else { fatalError("Error encoding data") }
            do {
                let outfile = Self.fileURL
                try data.write(to: outfile)
            } catch {
                fatalError("Can't write to file")
            }
        }
    }
    
    func loadCachedDelivery() {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let data = try? Data(contentsOf: Self.fileURL) else {
                return
            }
            
            guard let cachedDelivery = try? JSONDecoder().decode(Delivery.self, from: data) else {
                fatalError("Can't decode saved delivery data.")
            }
            
            DispatchQueue.main.async {
                self?.delivery = cachedDelivery
            }
        }
    }
    
}

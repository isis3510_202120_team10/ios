//
//  PreviousDeliveryData.swift
//  WikiWiki
//
//  Created by Carlos Infante on 22/11/21.
//

import Foundation
import Firebase

final class PreviousDeliveryData: ObservableObject {
    let db = Firestore.firestore()
    @Published var delivery: Delivery?
    @Published var prevDeliveries: [PrevDelivery] = []
    
    private static var documentsFolder: URL {
        do {
            return try FileManager.default.url(for: .documentDirectory,
                                                  in: .userDomainMask,
                                                  appropriateFor: nil,
                                                  create: false)
        } catch {
            fatalError("Can't find documents directory.")
        }
    }
    
    private static var fileURL: URL {
        return documentsFolder.appendingPathComponent("previousDelivery.data")
    }
    
    func fetchAllDeliveries() {
        db.collection("delivery").addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                print("No documents")
                return
            }
            self.prevDeliveries = documents.map { queryDocumentSnapshot -> PrevDelivery in
                let data = queryDocumentSnapshot.data()
                
                let clientName = data["clientName"] as? String ?? ""
                let deliveryId = data["orderID"] as? String ?? ""
                let startDate = (data["startDate"] as? Timestamp)?.dateValue() ?? Date()
                let finishDate = (data["finishDate"] as? Timestamp)?.dateValue() ?? Date()
                let diffs = Calendar.current.dateComponents([.hour, .minute], from: startDate, to: finishDate)
                let duration = (diffs.hour! * 60) + diffs.minute!
                
                return PrevDelivery(id: .init(), clientName: clientName, deliveredDate: startDate, duration: duration, orderID: deliveryId)
            }
            self.save()
        }
    }
    
    func load() {
        DispatchQueue.global(qos: .background).async{ [weak self] in
            guard let data = try? Data(contentsOf: Self.fileURL) else {
                return
            }
            guard let dailyPrevDeliveries = try? JSONDecoder().decode([PrevDelivery].self, from: data) else {
                fatalError("Can't decode saved previous delivery data.")
            }
            DispatchQueue.main.async {
                self?.prevDeliveries = dailyPrevDeliveries
            }
        }
    }
    
    func save() {
            DispatchQueue.global(qos: .background).async { [weak self] in
                guard let prevDeliveries = self?.prevDeliveries else { fatalError("Self out of scope") }
                guard let data = try? JSONEncoder().encode(prevDeliveries) else { fatalError("Error encoding data") }
                do {
                    let outfile = Self.fileURL
                    try data.write(to: outfile)
                } catch {
                    fatalError("Can't write to file")
                }
            }
        }
    
    func loadDelivery(documentName:String) {
        db.collection("delivery").document(documentName).getDocument { (document, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            }
            
            if let doc = document {
                do {
                    self.delivery = try doc.data(as: Delivery.self)
                    self.saveToCache()
                } catch {
                    print("Error Mapping Document: \(error)")
                }
            }
            else {
                print("Error. no document")
            }
        }
    }
    
    func saveToCache() {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let currentDelivery = self?.delivery else { fatalError("Self out of scope") }
            guard let data = try? JSONEncoder().encode(currentDelivery) else { fatalError("Error encoding data") }
            do {
                let outfile = Self.fileURL
                try data.write(to: outfile)
            } catch {
                fatalError("Can't write to file")
            }
        }
    }
    
    func loadCachedDelivery() {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let data = try? Data(contentsOf: Self.fileURL) else {
                return
            }
            
            guard let cachedDelivery = try? JSONDecoder().decode([PrevDelivery].self, from: data) else {
                fatalError("Can't decode saved delivery data.")
            }
            
            DispatchQueue.main.async {
                self?.prevDeliveries = cachedDelivery
            }
        }
    }
    
}

//
//  DeliveryDetailView.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 8/12/21.
//

import Foundation
import Firebase

final class DeliveryDetailData: ObservableObject {
    let db = Firestore.firestore()
    @Published var delivery: Delivery?
    @Published var notFound = false
    
    private static var documentsFolder: URL {
        do {
            return try FileManager.default.url(for: .documentDirectory,
                                               in: .userDomainMask,
                                               appropriateFor: nil,
                                               create: false)
        } catch {
            fatalError("Can't find documents directory.")
        }
    }

    func loadDelivery(orderId: String) {
        db.collection("delivery").whereField("orderID", isEqualTo: orderId).getDocuments { (result, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            }
        
            if let doc = result?.documents[0] {
                do {
                    self.delivery = try doc.data(as: Delivery.self)
                    self.saveToCache(orderId: orderId)
                } catch {
                    print("Error Mapping Document: \(error)")
                }
            }
            else {
                print("Error. no document")
            }
        }
    }
    
    func saveToCache(orderId: String) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let currentDelivery = self?.delivery else { fatalError("Self out of scope") }
            guard let data = try? JSONEncoder().encode(currentDelivery) else { fatalError("Error encoding data") }
            do {
                let outfile = Self.documentsFolder.appendingPathComponent("DeliveryDetail-" + orderId + ".data")
                try data.write(to: outfile)
            } catch {
                fatalError("Can't write to file")
            }
        }
    }
    
    func loadCachedDelivery(orderId: String) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let data = try? Data(contentsOf: Self.documentsFolder.appendingPathComponent("DeliveryDetail-" + orderId + ".data")) else {
                self?.notFound = true
                return
            }
            
            guard let cachedDelivery = try? JSONDecoder().decode(Delivery.self, from: data) else {
                fatalError("Can't decode saved delivery data.")
            }
            
            DispatchQueue.main.async {
                self?.delivery = cachedDelivery
            }
        }
    }
    
    func formatDate(date: Date?) -> String {
        if let d = date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/YY HH:mm:ss"
            return dateFormatter.string(from: d)
        } else {
            return ""
        }
    }
    
    func getDifferenceInMinutes(start: Date?, end: Date?) -> Int? {
        if let s = start, let e = end {
            return Calendar.current.dateComponents([.minute], from: s, to: e).minute
        } else {
            return 0
        }
    }
}

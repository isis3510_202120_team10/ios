//
//  Restaurant.swift
//  WikiWiki
//
//  Created by Carlos Infante on 3/11/21.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore


public struct Restaurant: Codable {
    let id: String
    let name: String
    let photoUrl: String
    
    init(id: String, name: String, photoUrl: String = "photo") {
        self.id = id
        self.name = name
        self.photoUrl = photoUrl
    }
}

//
//  Driver.swift
//  WikiWiki
//
//  Created by Carlos Infante on 8/12/21.
//

import Foundation

struct Driver: Identifiable, Hashable, Codable {
    var id: String = UUID().uuidString
    var lastName: String
    var name: String
    var phoneNumber: String
    var role: String
}

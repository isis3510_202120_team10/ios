//
//  Delivery.swift
//  WikiWiki
//
//  Created by Pietro Ehrlich on 9/10/21.
//

import Foundation
import CoreLocation
import FirebaseFirestore
import FirebaseFirestoreSwift

struct DeliveryLocation: Codable {
    var address: String
    var coordinate: GeoPoint
}

struct Delivery: Codable {
    var clientName: String
    var deliveryOrigin: DeliveryLocation
    var deliveryDestination: DeliveryLocation
    var status: Int
    var orderID: String
    var phone: String
    var startDate: Timestamp
    var finishDate: Timestamp
}


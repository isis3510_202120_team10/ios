//
//  DriverDelivery.swift
//  WikiWiki
//
//  Created by Carlos Infante on 8/12/21.
//

import Foundation

struct DriverDelivery: Identifiable, Hashable, Codable {
    var id: String = UUID().uuidString
    var driverName: String
    var duration: Int
    var orderID: String
    var status: Int
}

//
//  PreviousDelivery.swift
//  WikiWiki
//
//  Created by Carlos Infante on 22/11/21.
//

import Foundation
import CoreLocation
import FirebaseFirestore
import FirebaseFirestoreSwift

struct PrevDelivery: Identifiable, Hashable, Codable {
    var id: String = UUID().uuidString
    var clientName: String
    var deliveredDate: Date
    var duration: Int
    var orderID: String
    var dateString: String {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-yyyy HH:mm"
            return formatter.string(from: deliveredDate)
        }
}

extension PrevDelivery {
    static var data: [PrevDelivery] {
        [
            PrevDelivery(id: "dkfsa8f0usduaf", clientName: "John Wick", deliveredDate: Date(), duration: 25, orderID: "abc123"),
            PrevDelivery(id: "fdsaf23qrdfcdsafs", clientName: "Vanessa Hilton", deliveredDate: Date(), duration: 30, orderID: "uiadj873214"),
            PrevDelivery(id: "id9782yhndiuchy8wu3", clientName: "Barack Obama", deliveredDate: Date(), duration: 15, orderID: "8ads7y76c")
        ]
    }
}

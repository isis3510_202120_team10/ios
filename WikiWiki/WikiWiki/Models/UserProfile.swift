//
//  UserProfile.swift
//  WikiWiki
//
//  Created by Jorge Andrés Esguerra Alarcón on 10/24/21.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore

// corresponde a un documento de firebase
// USER
// El model se encarga de traducir lo que llega del JSON o de la DB a un objeto.
// Cosas que SI van aca: Formatear last name
// agregar un dato al phone number.
// REquests y lo demás si va al otra 

public class UserProfile: Codable {
    let name: String
    let lastName: String
    let id: String
    let phoneNumber: String
    let role: String // TODO: update when roles are defined!
    let photoUrl: String
    
    enum CodingKeys: String, CodingKey {
        case name
        case lastName
        case id
        case phoneNumber
        case role
        case photoUrl = "photo"
    }
}

